from rest_framework.permissions import IsAuthenticatedOrReadOnly


class IsOrganiserOfGameOrReadOnly(IsAuthenticatedOrReadOnly):
    def has_object_permission(self, request, view, obj):
        for group in request.user.groups.all():
            if obj.game.name == str(group):
                return True
        return False
