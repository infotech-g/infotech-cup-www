import logging
from rest_framework.views import exception_handler
from rest_framework.response import Response
from rest_framework.exceptions import ValidationError
from rest_framework import status
from .models import Game
from teams.models import Team
from matches.models import Status


class NoGameParamError(ValidationError):
    pass


def custom_exception_handler(exc, context):
    response = exception_handler(exc, context)

    if isinstance(exc, Game.DoesNotExist):
        err_data = {'detail': 'Game not exist'}
        logging.error(f"Original error detail and callstack: {exc}")
        return Response(err_data, status.HTTP_404_NOT_FOUND)

    if isinstance(exc, Team.DoesNotExist):
        err_data = {'detail': 'Team not exist'}
        logging.error(f"Original error detail and callstack: {exc}")
        return Response(err_data, status.HTTP_404_NOT_FOUND)

    if isinstance(exc, Status.DoesNotExist):
        err_data = {'detail': 'Status not exist'}
        logging.error(f"Original error detail and callstack: {exc}")
        return Response(err_data, status.HTTP_404_NOT_FOUND)

    if isinstance(exc, NoGameParamError):
        err_data = {'detail': 'Game parameter is required'}
        logging.error(f"Original error detail and callstack: {exc}")
        return Response(err_data, status.HTTP_404_NOT_FOUND)

    return response


def check_does_exist(game_data):
    Game.objects.get(name=game_data.get('name'))


def has_group_permission(request):
    for group in request.user.groups.all():
        if request.data.get('game').get('name') == str(group):
            return True
    return False
