import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Router } from '@angular/router';
import { TokenStorageService } from './token-storage.service';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

  constructor(private router: Router, private tokenService: TokenStorageService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let bearer = "Not logged in"

    const idToken = localStorage.getItem("id_token");
    if (idToken) {
      bearer = idToken
    }

    const cloned = req.clone({
      headers: req.headers.set("Authorization",
          "Bearer " + bearer)
    });

    return next.handle(cloned).pipe(
      catchError((error) => {
        if (error.status === 401) {
          this.router.navigate(['/login'])
          this.tokenService.signOut()
        }
        return throwError(error)
      })
    )
  }
}
