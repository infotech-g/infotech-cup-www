import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faPen, faTrash } from '@fortawesome/free-solid-svg-icons';
import { CrudService } from 'src/app/crud.service';
import { Group } from 'src/app/models/group';
import { Match } from 'src/app/models/match';

@Component({
  selector: 'app-groups',
  templateUrl: './groups.component.html',
  styleUrls: ['./groups.component.scss']
})
export class GroupsComponent implements OnInit {

  selected_game: string = "";
  groups: Group[] = [];
  matches: Match[] = [];

  faPen = faPen
  faTrash = faTrash

  constructor(
    private route: ActivatedRoute,
    private crudService: CrudService
  ) { }

  ngOnInit(): void {
    this.selected_game = this.route.snapshot.url[0].path
    this.getData()
  }

  getData(): void {
    this.crudService.getGroups(this.selected_game).subscribe(
      (groups: Group[]) => {
        this.groups = groups

        this.crudService.getMatches(this.selected_game).subscribe(
          (matches: Match[]) => {
            this.matches = matches
        })
    })
  }
}
