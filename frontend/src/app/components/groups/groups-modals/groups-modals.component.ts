import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { faExclamationTriangle, faTimes } from '@fortawesome/free-solid-svg-icons';
import { GroupedObservable } from 'rxjs';
import { CrudService } from 'src/app/crud.service';
import { Group } from 'src/app/models/group';
import { GroupOptions } from 'src/app/models/group_options';
import { Team } from 'src/app/models/team';
import { GroupAssignment } from 'src/app/models/teamInGroup';
import { TeamsComponent } from '../../teams/teams.component';
import { GroupsComponent } from '../groups.component';

@Component({
  selector: 'app-groups-modals',
  templateUrl: './groups-modals.component.html',
  styleUrls: ['./groups-modals.component.scss']
})
export class GroupsModalsComponent implements OnInit{

  // Icons
  faTimes = faTimes
  faError = faExclamationTriangle

  // Variables
  show_modal: boolean = false;
  which_modal: string = "";
  groups_size!: number;
  edit_form!: FormGroup;
  teams: Team[] = [];
  target_group: any

  // Form template
  form: FormGroup = this.formBuilder.group({
    number_of_groups: ["", [Validators.required, Validators.max(24)]],
    size_of_groups: ["", [Validators.required, Validators.max(24)]],
    teams_passing: ["", [Validators.required, Validators.max(24)]]
  });
  

  constructor(
    private formBuilder: FormBuilder,
    private crudService: CrudService,
    private route: ActivatedRoute,
    private groupsComponent: GroupsComponent
    ) { }

  ngOnInit(): void {
    this.crudService.getTeams(this.route.snapshot.url[0].path).subscribe(
      (teams: Team[]) => {
        this.teams = teams
    })
  }

  open(target: string, options?: {group: Group}): void {
    if(options) {
      this.setEditForm(options.group.assignments)
      this.groups_size = options.group.assignments.length
      this.target_group = options.group
    }
    this.which_modal = target
    this.show_modal = true
  }

  close(): void {
    this.show_modal = false;
  }

  onSubmit(): void {    
    if (this.which_modal === "generate") {
      if(this.form.invalid) {
        console.log("Form is invalid")
      } else {
        let selected_game = this.route.snapshot.url[0].path
        let group_options: GroupOptions = <GroupOptions>this.formatData(this.form)

        this.crudService.createGroups(group_options, selected_game).subscribe(
          data => {
            console.log(data)
            this.close()
            this.groupsComponent.ngOnInit()
          }
        )
      }
    } else if (this.which_modal === "edit") {
      if(this.edit_form.invalid) {
        console.log("Form is invalid")
      } else {
        let group_data: Group = <Group>this.formatData(this.edit_form)
        this.crudService.updateGroup(group_data, this.target_group.id).subscribe(
          data => {
            this.close()
            this.groupsComponent.ngOnInit()
          }
        )
      }
    }
  }

  formatData(data: FormGroup): Object {
    let formated_data = {};

    if (this.which_modal === 'generate') {
      formated_data = {
        number_of_groups: data.get('number_of_groups')?.value,
        size_of_groups: data.get('size_of_groups')?.value,
        teams_passing: data.get('teams_passing')?.value
      }
    } 

    else if (this.which_modal === 'edit') {
      let assignments: any = [];

      for (let i=1; i <= this.groups_size; i++) {
        let team_name: string = ""
        let assignment_id = this.target_group.assignments[i-1].id
        this.teams.forEach(team => {
          if (team.id == data.get(`team_${i}`)?.value) {
            team_name = team.name
            assignments.push({
              id: assignment_id,
              team: {
                id: data.get(`team_${i}`)?.value,
                name: team_name
              },
              score: data.get(`score_${i}`)?.value
            })
          }
        });
      }

      formated_data = {
        id: this.target_group.id,
        name: this.target_group.name,
        game: this.target_group.game,
        assignments
      }
    }

    return formated_data
  }
  
  setEditForm(assignments: GroupAssignment[]): void {
    this.edit_form = this.formBuilder.group({
    })
    
    for (let i = 1; i <= assignments.length; i++) {
      this.edit_form.addControl(
        'team_' + (i),
        new FormControl(
          assignments[i-1].team ? assignments[i-1].team.name : ""
        )
      )
      this.edit_form.addControl(
        'score_' + (i),
        new FormControl(
          ""
        )
      )
    }
  }

  getErrorMessage(control_name: string): string {
    let error_msg: string = ""
    let form_control = this.form.get(control_name)
    
    if (form_control?.hasError('required')) { // If is empty and has requierd validator
      error_msg = "To pole jest wymagane"
    } else if (form_control?.hasError('max')) {
      error_msg = "Maksymalna wartość to 24"
    }
    return error_msg
  }

  isInvalid(name: string): boolean {
    if (this.form.get(name)?.touched && this.form.get(name)?.invalid) {
      return true
    } else {
      return false
    } 
  }

  counter(i: number) {
    return new Array(i)
  } 
}
