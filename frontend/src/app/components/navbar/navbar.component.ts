import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { CrudService } from 'src/app/crud.service';
import { TokenStorageService } from 'src/app/token-storage.service';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  selected_game: string = "";

  constructor(
    private route: ActivatedRoute,
    private crudService: CrudService,
    private tokenService: TokenStorageService
  ) { }

  ngOnInit(): void {
    this.selected_game = this.route.snapshot.url[0].path
  }

  changeGame(game: string): void {
    this.selected_game = game
  }

  logout(): void {
    this.tokenService.signOut()
  }
}
