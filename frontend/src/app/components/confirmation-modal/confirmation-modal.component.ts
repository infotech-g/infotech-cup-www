import { Component, OnInit } from '@angular/core';
import { faTimes } from '@fortawesome/free-solid-svg-icons';
import { CrudService } from 'src/app/crud.service';
import { TeamsComponent } from '../teams/teams.component';

@Component({
  selector: 'confirmation-modal',
  templateUrl: './confirmation-modal.component.html',
  styleUrls: ['./confirmation-modal.component.scss']
})
export class ConfirmationModalComponent {

  // Icons
  faTimes = faTimes

  // Variables
  show_modal: boolean = false;
  type: string = "";
  id!: number;

  constructor(private crud: CrudService, private teamsComponent: TeamsComponent) {}

  open(type: string, id: number): void {
    this.type = type
    this.id = id
    this.show_modal = true
  }

  close(submit: boolean): void {
    this.show_modal = false

    if(submit) {
      this.crud.deleteResource(this.type, this.id).subscribe(
        res => {
          if (this.type === "teams") {
            this.teamsComponent.ngOnInit()
          } else if (this.type === "matches") {
            console.log('refresh matches')
          }
        }
      )
    }
  }
}
