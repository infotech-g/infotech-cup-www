import { Component } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { faExclamationTriangle, faTimes } from '@fortawesome/free-solid-svg-icons';
import { CrudService } from 'src/app/crud.service';
import { Team } from 'src/app/models/team';
import { TeamsComponent } from '../teams.component';

@Component({
  selector: 'app-teams-modals',
  templateUrl: './teams-modals.component.html',
  styleUrls: ['./teams-modals.component.scss'],
})
export class TeamsModalsComponent{

  // Icons
  faTimes = faTimes
  faError = faExclamationTriangle

  // Variables
  show_modal: boolean = false;
  which_modal: string = "";
  team_id!: number;

  // Form template
  form: FormGroup = this.formBuilder.group({
    name: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
    captain: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
    member_2: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
    member_3: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
    member_4: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
    member_5: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
    member_6: ["", [Validators.minLength(3), Validators.maxLength(20)]],
    game: [""],
    captain_id: [""],
    member_2_id: [""],
    member_3_id: [""],
    member_4_id: [""],
    member_5_id: [""],
    member_6_id: [""],
  });

  constructor(private formBuilder: FormBuilder, private crudService: CrudService,
              private route: ActivatedRoute, private teamsComponent: TeamsComponent) { }

  open(target: string, options?: {game?: string, team?: Team}): void {
    if(options?.team !== undefined) { // If team is passed sets its values to form
      console.log(options.team)
      this.setValues(options.team)
      this.team_id = options.team.id
    } else if (options?.game !== undefined) { // If game is passed sets its value to form
      this.form.patchValue({game: options.game})
    }
    this.which_modal = target
    this.show_modal = true
  }

  close(): void {
    this.show_modal = false
    this.form.reset()
  }

  onSubmit(): void {
    if(this.form.invalid) {
      console.log("Form is invalid")
    } else {
      let selected_game = this.route.snapshot.url[0].path
      let team_data: Team = <Team>this.formatData(this.form)
      
      if (this.which_modal === "add") {
        this.crudService.createTeam(team_data).subscribe(
          data => {
            this.close()
            this.teamsComponent.ngOnInit()
          }
        )
      } else if (this.which_modal === "edit") {
        this.crudService.updateTeam(team_data, this.team_id).subscribe(
          data => {
            this.close()
            this.teamsComponent.ngOnInit()
          }
        )
      }
    }
  }

  formatData(data: FormGroup): Object {
    let formated_data: any;

    if (this.which_modal === 'edit') {
      formated_data = {
        name: data.get('name')?.value,
        game: {name: data.get('game')?.value},
        captain: {id: data.get('captain_id')?.value ,name: data.get('captain')?.value},
        members: [
          {id: data.get('captain_id')?.value ,name: data.get('captain')?.value},
          {id: data.get('member_2_id')?.value, name: data.get('member_2')?.value},
          {id: data.get('member_3_id')?.value, name: data.get('member_3')?.value},
          {id: data.get('member_4_id')?.value, name: data.get('member_4')?.value},
          {id: data.get('member_5_id')?.value, name: data.get('member_5')?.value},
        ]
      }
    } else {
      formated_data = {
        name: data.get('name')?.value,
        game: {name: data.get('game')?.value},
        captain: {name: data.get('captain')?.value},
        members: [
          {name: data.get('captain')?.value},
          {name: data.get('member_2')?.value},
          {name: data.get('member_3')?.value},
          {name: data.get('member_4')?.value},
          {name: data.get('member_5')?.value},
        ]
      }
    }


    // Checks if member_6 is not empty, if it's not than adds it's value to members list
    if(data.get('member_6')?.value) {
      if (this.which_modal === 'edit') {
        formated_data.members.push({id: data.get('member_6_id')?.value, name: data.get('member_6')?.value})
      } else {
        formated_data.members.push({name: data.get('member_6')?.value})
      }
    }

    return formated_data
  }

  setValues(team: Team): void {
    // Creates list of members without captain
    let members = [];
    for(let i=0; i<team.members.length; i++) {
      if(team.members[i].name !== team.captain.name) {
        members.push(team.members[i])
      }
    }

    this.form.setValue({
      name: team.name,
      game: team.game.name,
      captain: team.captain.name,
      captain_id: team.captain.id,
      member_2: members[0] ? members[0].name : "",
      member_2_id: members[0] ? members[0].id : null,
      member_3: members[1] ? members[1].name : "",
      member_3_id: members[1] ? members[1].id : null,
      member_4: members[2] ? members[2].name : "",
      member_4_id: members[2] ? members[2].id : null,
      member_5: members[3] ? members[3].name : "",
      member_5_id: members[3] ? members[3].id : null,
      member_6: members[4] ? members[4].name : "",
      member_6_id: members[4] ? members[4].id : null,
    })
  }
  
  getErrorMessage(control_name: string): string {
    let error_msg: string = ""
    let form_control = this.form.get(control_name)
    
    if (form_control?.hasError('required')) { // If is empty and has requierd validator
      error_msg = "To pole jest wymagane"
    } else if (form_control?.hasError('minlength')) { // If has less characters than required and has minlenght validator
      error_msg = "To pole powinno mieć co najmniej " + form_control.errors?.minlength.requiredLength + " znaków"
    } else if (form_control?.hasError('maxlength')) {  // If has more characters than required and has maxlenght validator
      error_msg = "To pole może mieć maksymalnie " + form_control.errors?.maxlength.requiredLength + " znaków"
    } 

    return error_msg
  }

  isInvalid(name: string): boolean {
    // Function checks, if given field is invalid and has been touched.
    // If yes, returns true. Else returns false. 
    
    if (this.form.get(name)?.touched && this.form.get(name)?.invalid) {
      return true
    } else {
      return false
    } 
  }
}