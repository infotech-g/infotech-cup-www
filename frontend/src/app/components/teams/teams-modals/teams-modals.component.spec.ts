import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamsModalsComponent } from './teams-modals.component';

describe('TeamsModalsComponent', () => {
  let component: TeamsModalsComponent;
  let fixture: ComponentFixture<TeamsModalsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamsModalsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamsModalsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
