import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { faPen, faTrash } from '@fortawesome/free-solid-svg-icons';
import { CrudService } from 'src/app/crud.service';
import { Team } from 'src/app/models/team';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.scss']
})
export class TeamsComponent implements OnInit {

  selected_game: string = "";

  teams: Team[] = [];

  faTrash = faTrash
  faPen = faPen

  constructor(
    private route: ActivatedRoute,
    private crudService: CrudService,
  ) { }

  ngOnInit(): void {
    this.selected_game = this.route.snapshot.url[0].path
    this.getData()
  }

  getData(): void {
    this.crudService.getTeams(this.selected_game).subscribe(
      (teams: Team[]) => {
        this.teams = teams
      }
    )
  }
}
