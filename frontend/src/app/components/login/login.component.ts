import { Component, OnInit } from '@angular/core';
import { TokenStorageService } from 'src/app/token-storage.service';
import { AuthService } from 'src/app/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private formBuilder: FormBuilder, private authService: AuthService, private tokenStorage: TokenStorageService, private route:Router) { }
  
  form: FormGroup = this.formBuilder.group({
    username: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
    password: ["", [Validators.required, Validators.minLength(3), Validators.maxLength(20)]],
  });

  ngOnInit(): void {
  }

  onSubmit(): void {
    const username: string = this.form.get('username')?.value;
    const password: string = this.form.get('password')?.value;
    this.authService.login(username, password).subscribe(
      data => {
        this.tokenStorage.saveToken(data.access);
        this.route.navigate(['/teams/league-of-legends']);
      },
      err => {
        console.log(err.error.message)
        //   this.errorMessage = err.error.message;
     //   this.isLoginFailed = true;
      }
    );
  }
}
