import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { TokenStorageService } from './token-storage.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'AngularApp';
  constructor(tokenStorageService: TokenStorageService, router: Router) {
    const isLoggedIn = !!tokenStorageService.getToken()
    if (!isLoggedIn) {
      router.navigate(['/login']);
    }
  }
}
