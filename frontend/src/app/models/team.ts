import { Game } from "./game";
import { Player } from "./player";

export interface Team {
    id: number,
    name: string,
    game: Game,
    captain: Player,
    members: Player[]
}