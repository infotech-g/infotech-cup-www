import { Team } from "./team";

export interface GroupAssignment {
    id: number,
    team: Team,
    score: string
}