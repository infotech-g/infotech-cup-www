export interface GroupOptions{
    "number_of_groups": number,
    "size_of_groups": number,
    "teams_passing": number
}