import { GroupAssignment } from "./teamInGroup";

export interface Group {
    id: number,
    name: string,
    game: string,
    size: number,
    teams_passing: number,
    assignments: GroupAssignment[]
}