export interface Match {
    id: number,
    time: string,
    date: string,
    status: string,
    result: string,
    team1Name: string,
    team2Name: string
}