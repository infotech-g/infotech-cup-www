import { Injectable } from '@angular/core';

import { HttpClient} from '@angular/common/http';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

import { Team } from './models/team';
import { Group } from './models/group';
import { Match } from './models/match';
import { GroupOptions } from './models/group_options';

const baseUrl = "http://localhost:8000";

@Injectable({
  providedIn: 'root'
})
export class CrudService {

  constructor(
    private http: HttpClient
    ) { }

  deleteResource(resource: string, id: number): Observable<any> {
    const url = `${baseUrl}/${resource}/${id}`
    return this.http.delete(url)
  }

  /**
   * TEAMS Section
   */

  getTeams(selected_game: string): Observable<Team[]> {
    const url = `${baseUrl}/teams`
    return this.http.get<Team[]>(url, {params: {game: selected_game}})
  }

  createTeam(team_data: Team): Observable<Team> {
    const url = `${baseUrl}/teams`
    return this.http.post<Team>(url, team_data)
  }

  updateTeam(team_data: Team, team_id: number): Observable<Team> {
    console.log(team_data)
    const url = `${baseUrl}/teams/${team_id}`
    return this.http.put<Team>(url, team_data)
  }

  /**
   * GROUPS Section
   */

  getGroups(selected_game: string): Observable<Group[]> {
    const url = `${baseUrl}/groups`
    return this.http.get<Group[]>(url, {params: {game: selected_game}})
  }

  createGroups(options: GroupOptions, selected_game: string): Observable<Group[]> {
    const url = `${baseUrl}/groups`
    return this.http.post<Group[]>(url, options, {params: {game: selected_game}})

  }

  updateGroup(group_data: Group, group_id: number): Observable<Group> {
    console.log(group_data)
    const url = `${baseUrl}/groups/${group_id}`
    return this.http.put<Group>(url, group_data)
  }

  /**
   * MATCHES Section
   */

  getMatches(selected_game: string): Observable<Match[]> {
    const url = `${baseUrl}/matches`
    return this.http.get<Match[]>(url, {params: {game: selected_game}})
  }
}
