import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GroupsComponent } from './components/groups/groups.component';
import { LoginComponent } from './components/login/login.component';
import { PageNotFoundComponent } from './components/page-not-found/page-not-found.component';
import { PlayoffsComponent } from './components/playoffs/playoffs.component';
import { TeamsComponent } from './components/teams/teams.component';

const routes: Routes = [
  {
    path: "teams",
    children: [
      {
        path: "league-of-legends",
        component: TeamsComponent,
      }, {
        path: "valorant",
        component: TeamsComponent,
      }, {
        path: "csgo",
        component: TeamsComponent,
      }
    ]
  }, {
    path: "groups",
    children: [
      {
        path: "league-of-legends",
        component: GroupsComponent,
      }, {
        path: "valorant",
        component: GroupsComponent,
      }, {
        path: "csgo",
        component: GroupsComponent,
      }
    ]
  }, {
    path: "playoffs",
    children: [
      {
        path: "league-of-legends",
        component: PlayoffsComponent,
      }, {
        path: "valorant",
        component: PlayoffsComponent,
      }, {
        path: "csgo",
        component: PlayoffsComponent,
      }
    ]
  }, {
    path: "login",
    component: LoginComponent
  }, {
    path: "",
    redirectTo: "/login",
    pathMatch: "full"
  }, {
    path: "**",
    component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
