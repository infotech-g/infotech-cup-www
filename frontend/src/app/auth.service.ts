import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from 'src/app/models/user'
import { tap, shareReplay } from 'rxjs/operators';
import * as moment from "moment";
import { TokenData } from 'src/app/models/token_data';

const AUTH_API = 'http://localhost:8000/';
const httpOptions = {
    headers: new HttpHeaders({ 
      'Content-Type': 'application/json',
    })
  };

@Injectable()
export class AuthService {

    constructor(private http: HttpClient) { }

    login(username:string, password:string) {
        return this.http.post<TokenData>(AUTH_API + 'token', {
            username,
            password
        }, httpOptions).pipe(
              shareReplay(),
              tap(
                token => this.setSession(token.access)
              ),
            )
    }
          
    private setSession(idToken: string) {
        localStorage.setItem('id_token', idToken);
    }          

    logout() {
        localStorage.removeItem("id_token");
        localStorage.removeItem("expires_at");
    }
}