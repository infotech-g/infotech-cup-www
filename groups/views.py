from rest_framework.generics import ListCreateAPIView, RetrieveUpdateAPIView
from rest_framework.response import Response
from core.models import Game
from core.utils import NoGameParamError
from .serializers import GroupSerializer, OptionsSerializer
from .models import GroupAssignment, Group


class GroupListCreateView(ListCreateAPIView):
    serializer_class = GroupSerializer
    queryset = Group.objects.all()

    def get_queryset(self):
        game_name = self.request.query_params.get('game')
        if game_name is not None:
            game = Game.objects.get(name=game_name)
            return Group.objects.filter(game=game)
        return Group.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = OptionsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        game_name = request.query_params.get('game')
        number_of_groups = serializer.validated_data.get('number_of_groups')
        size_of_groups = serializer.validated_data.get('size_of_groups')
        teams_passing = serializer.validated_data.get('teams_passing')

        if game_name is None:
            raise NoGameParamError

        Group.objects.filter(game__name=game_name).delete()
        groups_data = self.create_new_groups(game_name, number_of_groups, size_of_groups, teams_passing)
        return Response(groups_data)

    def create_new_groups(self, game_name, number_of_groups, size_of_groups, teams_passing):
        alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L',
                    'M', 'N', 'O', 'P', 'R', 'S', 'T', 'U', 'W', 'X', 'Y', 'Z']
        game = Game.objects.get(name=game_name)
        for i in range(number_of_groups):
            group = Group.objects.create(
                game=game,
                name="Group "+alphabet[i],
                size=size_of_groups,
                teams_passing=teams_passing
            )
            for j in range(size_of_groups):
                GroupAssignment.objects.create(
                    group=group,
                )
        groups = Group.objects.filter(game__name=game_name)
        serializer = GroupSerializer(groups, many=True)
        return serializer.data


class GroupRetrieveUpdateDestroyView(RetrieveUpdateAPIView):
    serializer_class = GroupSerializer
    queryset = Group.objects.all()
