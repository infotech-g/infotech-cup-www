from django.urls import path
from . import views


urlpatterns = [
    path('', views.GroupListCreateView.as_view(), name='group_list'),
    path('/<pk>', views.GroupRetrieveUpdateDestroyView.as_view(), name='group_detail'),
]
