from django.db import models
from core.models import Game
from teams.models import Team


class Group(models.Model):
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    name = models.CharField(max_length=50)
    size = models.IntegerField()
    teams_passing = models.IntegerField()

    def __str__(self):
        return self.name


class GroupAssignment(models.Model):
    team = models.ForeignKey(Team, on_delete=models.CASCADE, null=True, blank=True)
    group = models.ForeignKey(Group, related_name='assignments', on_delete=models.CASCADE)
    score = models.CharField(max_length=10, null=True, blank=True)

    def __str__(self):
        return "Group: " + str(self.group) + " | Team: " + str(self.team)
