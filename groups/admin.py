from django.contrib import admin
from .models import Group, GroupAssignment

admin.site.register(Group)
admin.site.register(GroupAssignment)
