from rest_framework import serializers
from core.serializers import GameSerializer
from teams.models import Team
from .models import Group, GroupAssignment


class TeamInGroupSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Team
        fields = ('id', 'name')


class GroupAssignmentSerializer(serializers.ModelSerializer):
    team = TeamInGroupSerializer(allow_null=True)
    id = serializers.IntegerField(required=False)

    class Meta:
        model = GroupAssignment
        fields = ('id', 'team', 'score')


class GroupSerializer(serializers.ModelSerializer):
    game = GameSerializer()
    assignments = GroupAssignmentSerializer(many=True, read_only=False)

    class Meta:
        model = Group
        fields = ('id', 'name', 'game', 'teams_passing', 'assignments')
        read_only_fields = ['name', 'game', 'teams_passing']

    def update(self, group, validated_data):
        assignments_data = validated_data.pop('assignments')
        for data in assignments_data:
            assignment = GroupAssignment.objects.get(id=data.get('id'))
            assignment.score = data.get('score')
            if data.get('team').get('name') != "":
                assignment.team = Team.objects.get(id=data.get('team').get('id'))
            assignment.save()
        return group


class OptionsSerializer(serializers.Serializer):
    number_of_groups = serializers.IntegerField()
    size_of_groups = serializers.IntegerField()
    teams_passing = serializers.IntegerField()
