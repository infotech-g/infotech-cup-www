from rest_framework import serializers
from core.models import Game
from core.serializers import GameSerializer
from .models import Team, Player


class PlayerSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField(required=False)

    class Meta:
        model = Player
        fields = ('id', 'name')


class TeamSerializer(serializers.ModelSerializer):
    game = GameSerializer(many=False)
    captain = PlayerSerializer(many=False)
    members = PlayerSerializer(many=True)

    class Meta:
        model = Team
        fields = ('id', 'name', 'game', 'captain', 'members')

    def create(self, validated_data):
        game_data = validated_data.pop('game')
        captain_data = validated_data.pop('captain')
        members_data = validated_data.pop('members')

        game = Game.objects.get(name=game_data.get('name'))

        members, captain = [], members_data[0]
        for member_data in members_data:
            player = Player.objects.create(**member_data)
            members.append(player)
            if member_data.get('name') == captain_data.get('name'):
                captain = player

        team = Team.objects.create(captain=captain,
                                   game=game,
                                   **validated_data)
        team.members.set(members)
        return team

    def update(self, team, validated_data):
        captain_data = validated_data.pop('captain')
        members_data = validated_data.pop('members')

        for member_data in members_data:
            player = Player.objects.get(id=member_data.get('id'))
            player.name = member_data.get('name')
            player.save()

        team.captain = Player.objects.get(id=captain_data.get('id'))
        team.name = validated_data.get('name')
        team.save()

        return team
