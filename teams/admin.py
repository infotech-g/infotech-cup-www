from django.contrib import admin
from teams.models import Player, Team

admin.site.register(Player)
admin.site.register(Team)
