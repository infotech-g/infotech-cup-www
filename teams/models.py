from django.db import models
from core.models import Game

# class Ladder(models.Model):
#     game_id = models.ForeignKey(Game, on_delete=models.CASCADE)
#     number_of_teams = models.IntegerField()
#
#     def __str__(self):
#         return "Ladder of " + str(self.game_id)


class Player(models.Model):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class Team(models.Model):
    name = models.CharField(max_length=50)
    game = models.ForeignKey(Game, on_delete=models.CASCADE, null=True)
    captain = models.ForeignKey(Player, on_delete=models.CASCADE)
    members = models.ManyToManyField(Player, related_name="members")

    def __str__(self):
        return self.name

#
# class MatchAssigment(models.Model):
#     match_id = models.ForeignKey(Match, on_delete=models.CASCADE)
#     #zrobić one-to-one
#     game_id = models.ForeignKey(Game, on_delete=models.CASCADE)
#     ladder_id = models.ForeignKey(Ladder, on_delete=models.CASCADE)
#     place_in_ladder = models.IntegerField()
