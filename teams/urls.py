from django.urls import path
from .views import TeamListCreateView, TeamDetailView


urlpatterns = [
    path('', TeamListCreateView.as_view(), name='team_list'),
    path('/<pk>', TeamDetailView.as_view(), name='team_detail'),
]
