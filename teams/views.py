from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.exceptions import PermissionDenied
from core.models import Game
from core.utils import check_does_exist, has_group_permission
from .models import Team
from .serializers import TeamSerializer


class TeamListCreateView(ListCreateAPIView):
    serializer_class = TeamSerializer

    def get_queryset(self):
        game_name = self.request.query_params.get('game')
        if game_name is not None:
            game = Game.objects.get(name=game_name)
            return Team.objects.filter(game=game)
        return Team.objects.all()

    def post(self, request, *args, **kwargs):
        game_data = request.data.get('game')
        check_does_exist(game_data)

        is_authorised = has_group_permission(request)
        if not is_authorised:
            raise PermissionDenied

        return self.create(request, *args, **kwargs)


class TeamDetailView(RetrieveUpdateDestroyAPIView):
    queryset = Team.objects.all()
    serializer_class = TeamSerializer
