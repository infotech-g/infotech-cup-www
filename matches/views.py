from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView
from rest_framework.exceptions import PermissionDenied
from core.utils import has_group_permission, check_does_exist
from core.models import Game
from .serializers import MatchSerializer
from .models import Match


class MatchListCreateView(ListCreateAPIView):
    serializer_class = MatchSerializer

    def get_queryset(self):
        game_name = self.request.query_params.get('game')
        if game_name is not None:
            game = Game.objects.get(name=game_name)
            return Match.objects.filter(game=game)
        return Match.objects.all()

    def post(self, request, *args, **kwargs):
        game_data = request.data.get('game')
        check_does_exist(game_data)

        is_authorised = has_group_permission(request)
        if not is_authorised:
            raise PermissionDenied

        return self.create(request, *args, **kwargs)


class MatchDetailView(RetrieveUpdateDestroyAPIView):
    queryset = Match.objects.all()
    serializer_class = MatchSerializer
