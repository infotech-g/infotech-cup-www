from django.db import models
from teams.models import Team
from groups.models import Game


class Status(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Match(models.Model):
    first_team = models.ForeignKey(
        Team, on_delete=models.CASCADE, related_name="first_team"
    )
    second_team = models.ForeignKey(
        Team, on_delete=models.CASCADE, related_name="second_team"
    )
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    result = models.CharField(max_length=10)
    date = models.DateField()
    time = models.TimeField()
    format = models.CharField(max_length=10)
    status = models.ForeignKey(Status, on_delete=models.CASCADE)
    isPlayoffs = models.BooleanField()

    def __str__(self):
        return str(self.first_team) + " vs " + str(self.second_team)
