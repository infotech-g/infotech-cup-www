from django.contrib import admin
from .models import Match, Status

admin.site.register(Match)
admin.site.register(Status)
