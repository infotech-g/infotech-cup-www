from rest_framework import serializers
from teams.models import Team
from core.serializers import GameSerializer
from core.models import Game
from .models import Match, Status


class TeamInMatchSerializer(serializers.ModelSerializer):
    id = serializers.IntegerField()

    class Meta:
        model = Team
        fields = ('id', 'name')
        read_only_fields = ('id', 'name',)


class StatusSerializer(serializers.ModelSerializer):

    class Meta:
        model = Status
        fields = ('name',)
        read_only_fields = ('name',)


class MatchSerializer(serializers.ModelSerializer):
    first_team = TeamInMatchSerializer()
    second_team = TeamInMatchSerializer()
    game = GameSerializer()
    status = serializers.StringRelatedField()

    class Meta:
        model = Match
        fields = ('id', 'first_team', 'second_team', 'game',
                  'result', 'date', 'time', 'format', 'status', 'isPlayoffs')

    def create(self, validated_data):
        first_team_data = validated_data.pop('first_team')
        second_team_data = validated_data.pop('second_team')
        game_data = validated_data.pop('game')

        first_team = Team.objects.get(id=first_team_data.get('id'))
        second_team = Team.objects.get(id=second_team_data.get('id'))
        game = Game.objects.get(name=game_data.get('name'))
        status = Status.objects.get(name='nieodbyty')

        match = Match.objects.create(first_team=first_team,
                                     second_team=second_team,
                                     game=game,
                                     result=validated_data.get('result'),
                                     date=validated_data.get('date'),
                                     time=validated_data.get('time'),
                                     format=validated_data.get('format'),
                                     status=status,
                                     isPlayoffs=validated_data.get('isPlayoffs'))
        return match

    def update(self, match, validated_data):
        first_team_data = validated_data.pop('first_team')
        second_team_data = validated_data.pop('second_team')

        first_team = Team.objects.get(id=first_team_data.get('id'))
        second_team = Team.objects.get(id=second_team_data.get('id'))
        status = Status.objects.get(name='nieodbyty')

        match.first_team = first_team
        match.second_team = second_team
        match.status = status
        match.save()

        return match
