from django.urls import path
from .views import MatchListCreateView, MatchDetailView


urlpatterns = [
    path('', MatchListCreateView.as_view(), name='match_list'),
    path('/<pk>', MatchDetailView.as_view(), name='team_detail'),
]
