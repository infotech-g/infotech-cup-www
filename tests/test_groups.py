from rest_framework.test import (
    APIRequestFactory,
    APITestCase,
    force_authenticate,
)
import pytest
from django.contrib.auth.models import Group, User
import json
from groups.views import GroupListCreateView
from core.models import Game
from .config import USERNAME, EMAIL, PASSWORD


class TestGroups(APITestCase):
    def setUp(self):
        global user
        user = User.objects.create_user(
            username=USERNAME, email=EMAIL, password=PASSWORD
        )
        user = User.objects.get(username=USERNAME)
        game = Game(name="lol")
        game.save()

        group = Group(name="lol")
        group.save()

        group = Group.objects.get(name="lol")
        group.user_set.add(1)
        group.save()

    @pytest.mark.order(9)
    def test_create_groups(self):
        payload = {"number_of_groups": 2, "size_of_groups": 2, "teams_passing": 2}
        factory = APIRequestFactory()
        view = GroupListCreateView.as_view()
        request = factory.post(
            "/groups/?game=lol", json.dumps(payload), content_type="application/json"
        )
        force_authenticate(request, user)
        response = view(request)
        self.create_group_response = json.dumps(response.data)
        assert response.status_code == 200

    @pytest.mark.order(10)
    def test_list_groups(self):
        factory = APIRequestFactory()
        view = GroupListCreateView.as_view()
        request = factory.get("/groups")
        force_authenticate(request, user)
        response = view(request)
        assert response.status_code == 200
