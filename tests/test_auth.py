from rest_framework.test import APIRequestFactory, APITestCase, force_authenticate

from django.contrib.auth.models import User
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView
import json
import pytest


from .config import USERNAME, EMAIL, PASSWORD


class TestAuth(APITestCase):
    refresh = ""

    def setUp(self):
        global user
        user = User.objects.create_user(
            username=USERNAME, email=EMAIL, password=PASSWORD
        )

    @pytest.mark.order(11)
    def test_no_data(self):
        payload = {}
        factory = APIRequestFactory()
        view = TokenObtainPairView.as_view()
        request = factory.post(
            "/token", json.dumps(payload), content_type="application/json"
        )
        response = view(request)
        assert response.status_code == 400

    @pytest.mark.order(12)
    def test_username_not_found(self):
        payload = {"username": "user", "password": "password"}
        factory = APIRequestFactory()
        view = TokenObtainPairView.as_view()
        request = factory.post(
            "/token", json.dumps(payload), content_type="application/json"
        )
        response = view(request)
        assert response.status_code == 401

    @pytest.mark.order(13)
    def test_obtain_token(self):
        payload = {"username": USERNAME, "password": PASSWORD}
        factory = APIRequestFactory()
        view = TokenObtainPairView.as_view()
        request = factory.post(
            "/token", json.dumps(payload), content_type="application/json"
        )
        response = view(request)
        TestAuth.refresh = str(response.data["refresh"])
        assert response.status_code == 200

    @pytest.mark.order(14)
    def test_refresh_token(self):
        payload = {"refresh": TestAuth.refresh}
        factory = APIRequestFactory()
        view = TokenRefreshView.as_view()
        request = factory.post(
            "/token", json.dumps(payload), content_type="application/json"
        )
        force_authenticate(request, user)
        response = view(request)
        assert response.status_code == 200
