import json

import pytest
from django.contrib.auth.models import Group, User
from rest_framework.test import (
    APIRequestFactory,
    APITestCase,
    force_authenticate,
)

from core.models import Game
from teams.views import TeamDetailView, TeamListCreateView
from .config import USERNAME, EMAIL, PASSWORD


class TestTeams(APITestCase):
    create_team_response = ""

    def setUp(self):
        global user
        user = User.objects.create_user(
            username=USERNAME, email=EMAIL, password=PASSWORD
        )
        user = User.objects.get(username=USERNAME)
        game = Game(name="lol")
        game.save()

        group = Group(name="lol")
        group.save()

        group = Group.objects.get(name="lol")
        group.user_set.add(1)
        group.save()

        payload = {
            "name": "fred",
            "game": {"name": "lol"},
            "captain": {"name": "plugh"},
            "members": [{"name": "qux"}, {"name": "plugh"}, {"name": "bar"}],
        }
        factory = APIRequestFactory()
        view = TeamListCreateView.as_view()
        request = factory.post(
            "/teams", json.dumps(payload), content_type="application/json"
        )
        force_authenticate(request, user)
        response = view(request)
        TestTeams.create_team_response = response.data

    @pytest.mark.order(1)
    def test_unauthorized_request(self):
        payload = {
            "name": "czacza",
            "game": {"name": "csgo"},
            "captain": {"name": "szorcu20"},
            "members": [
                {"name": "BIGFLORAS"},
                {"name": "szorcu20"},
                {"name": "smolFloras"},
            ],
        }
        factory = APIRequestFactory()
        view = TeamDetailView.as_view()
        request = factory.post(
            "/teams", json.dumps(payload), content_type="application/json"
        )
        response = view(request)
        assert response.status_code == 401

    @pytest.mark.order(2)
    def test_create_team(self):
        payload = {
            "name": "fred",
            "game": {"name": "lol"},
            "captain": {"name": "plugh"},
            "members": [{"name": "qux"}, {"name": "plugh"}, {"name": "bar"}],
        }
        factory = APIRequestFactory()
        view = TeamListCreateView.as_view()
        request = factory.post(
            "/teams", json.dumps(payload), content_type="application/json"
        )
        force_authenticate(request, user)
        response = view(request)
        TestTeams.create_team_response = response.data
        assert response.status_code == 201

    @pytest.mark.order(3)
    def test_list_teams_lol(self):
        factory = APIRequestFactory()
        view = TeamListCreateView.as_view()
        request = factory.get("/teams?game=lol", content_type="application/json")
        response = view(request)
        print(response.data)
        assert response.status_code == 200

    @pytest.mark.order(4)
    def test_update_team(self):
        payload = {
            "id": 1,
            "name": "FOOX",
            "game": {"name": "lol"},
            "captain": {"id": 2, "name": "plugh"},
            "members": [
                {"id": 1, "name": "qux"},
                {"id": 2, "name": "plugh"},
                {"id": 3, "name": "bar"},
            ],
        }
        factory = APIRequestFactory()
        view = TeamDetailView.as_view()
        request = factory.put(
            "/teams", json.dumps(payload), content_type="application/json"
        )
        force_authenticate(request, user)
        response = view(request, pk=1)
        print(response.data)
        assert response.status_code == 200

    @pytest.mark.order(5)
    def test_not_found(self):
        factory = APIRequestFactory()
        view = TeamDetailView.as_view()
        payload = {
            "id": 2,
            "name": "Grabówka",
            "captain": {"id": 5, "name": "BIGFLORAS"},
            "game": {"name": "lol"},
            "members": [
                {"id": 5, "name": "BIGFLORAS"},
                {"id": 6, "name": "szorcu20"},
                {"id": 7, "name": "xd"},
            ],
        }
        request = factory.put(
            "/teams", json.dumps(payload), content_type="application/json"
        )
        force_authenticate(request, user)
        response = view(request, pk=99)
        assert response.status_code == 404

    @pytest.mark.order(6)
    def test_delete_team(self):
        factory = APIRequestFactory()
        view = TeamDetailView().as_view()
        request = factory.delete("teams")
        force_authenticate(request, user)
        response = view(request, pk=1)
        print(response.data)
        assert response.status_code == 204
