"""
With these settings, tests run faster.
"""

from .base import *  # noqa
from .base import env
from .base import ROOT_DIR

# GENERAL
# ------------------------------------------------------------------------------
# https://docs.djangoproject.com/en/dev/ref/settings/#secret-key
SECRET_KEY = env(
    "DJANGO_SECRET_KEY",
    default="LdPPgst2tjN4IBgl0TbYvRqwZobIThQGb7Aq8cGejAlq5HC1eKpnCt4VMNMalqh2",
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ROOT_DIR / 'db.sqlite3',
    }
}
