rm -f db.sqlite3
python3 manage.py migrate
python3 manage.py makemigrations
DJANGO_SUPERUSER_PASSWORD=1 python3 manage.py createsuperuser --username admin --email admin@email.com --noinput
python3 manage.py runserver
